#!/usr/bin/env bash

set -eu
#!/usr/bin/env python 
#outputdir1=root://dcdoor11.usatlas.bnl.gov:1094//pnfs/usatlas.bnl.gov/atlasscratchdisk/rucio/user/jue/f3/80/user.jue.13047666.Akt10LCTopoTrmJets._000001.root
#outputdir=root://dcdoor11.usatlas.bnl.gov:1094//pnfs/usatlas.bnl.gov/atlasscratchdisk/rucio/user/jue/f3/80/user.jue.13047666.Akt10LCTopoTrmJets._000001.root
#root://eosatlas.cern.ch:1094//eos/atlas/atlasscratchdisk/rucio/user/jue/52/34/user.jue.13047752.Akt10LCTopoTrmJets._000001.root
#outputdir1=root://atlasxrootd-kit.gridka.de:1094//pnfs/gridka.de/atlas/disk-only/atlasscratchdisk/rucio/user/jue/91/6e/user.jue.13047711.Akt10LCTopoTrmJets._000010.root
#/user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000002.root
#outputdir=/eos/user/j/jue/user.jue.mc16_13TeV.361031.Pty8E_A14NNPDF23LO_jetjet_JZ11W.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047695.Akt10LCTopoTrmJets._000011.root


dijet=/eos/user/j/jue/user.jue.mc16_13TeV.361031.Pty8E_A14NNPDF23LO_jetjet_JZ11W.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047695.Akt10LCTopoTrmJets._000011.root

outputdir=/eos/user/j/jue/user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000001.root 

#root -l $outputdir/flav_Akt10LCTopoTrmJets_hhbbbb.root  
#python list_branch.py  --f $outputdir --t  bTag_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets 
python jetTree.py --filename $dijet --sig 0 --specify 1 --saveAs sig  #--num num_M1000.txt   --sig 1 #python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir --sig 1 --specify 1 --saveAs sig  #--num num_M1000.txt   --sig 1 #python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir\hhbbbb_M2000.root  --saveAs sig_M2000  --num num_M2000.txt   --sig 1 
#python jetTree.py --filename $outputdir\ttbar.root         --saveAs bkg_ttbar  --num num_ttbar.txt    #python jetTree.py --filename $outputdir\jetjet.root        --saveAs bkg_jetjet --num num_jetjet.txt   
##ipython trainBDT_split.py --sigfilenames  sig.root -     -bkdfilenames bkg.root 

