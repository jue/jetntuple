#!/bin/sh

#
myname=user.jue

#
# change the tag every time
#
tag=jetbased_0614

while read inDSName; do

  outDSName=${inDSName}.${tag}
  outDSName=${outDSName//ttbar-32-g71d105e_Akt10LCTopoTrmJets/} 
  outDSName=${outDSName//ttbar-28-gd2bd53a_Akt10LCTopoTrmJets/} 
  outDSName=${outDSName//ttbar-29-gb443dae_Akt10LCTopoTrmJets/} 
  outDSName=${outDSName//ttbar-33-g44c2e73_Akt10LCTopoTrmJets/} 
  outDSName=${outDSName//..v31-20-g0b7f375_Akt10LCTopoTrmJets/} 
  outDSName=${outDSName//..v31-18-g53934e9_Akt10LCTopoTrmJets/}    
  outDSName=${outDSName//ttbar-37-g7f8e471_Akt10LCTopoTrmJets/} #ttbar-34-gb5e402e_Akt10LCTopoTrmJets/}    

  echo $outDSName
  echo $inDSName
#  echo $outDSName
#  pathena --trf "Reco_tf.py --outputDAODFile pool.root --inputAODFile %IN  --reductionConf FTAG5" --inDS $inDSName --outDS $outDSName --inTarBall=$TARBALL  --nFilesPerJob=1

 # prun --exec "python jetTree.py --filename %IN --saveAs jet.root --specify 1 " --inDS $inDSName --outDS $outDSName --outputs out1.root --nFiles 1 
  prun --exec "python jetTree.py --filename %IN --specify 1 --sig 1" --inDS $inDSName --outDS $outDSName --outputs out1.root  --mergeOutput  --rootVer=6.10/04 --cmtConfig=x86_64-slc6-gcc62-opt  --nFiles=10

# use --nFilesPerJob=10 to control how many files per job0
# use --nFiles=10 to control how many files in total

#done <zprime_inputs.txt
#done< new_sig_inputs.txt   
#done< test.txt #new_sig_inputs.txt   
#done< new_sig.txt #bkg_inputs.txt   
done< new_sig

