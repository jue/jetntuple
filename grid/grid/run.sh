#!/usr/bin/env bash

set -eu
#!/usr/bin/env python 
#outputdir1=root://dcdoor11.usatlas.bnl.gov:1094//pnfs/usatlas.bnl.gov/atlasscratchdisk/rucio/user/jue/f3/80/user.jue.13047666.Akt10LCTopoTrmJets._000001.root
#outputdir=root://dcdoor11.usatlas.bnl.gov:1094//pnfs/usatlas.bnl.gov/atlasscratchdisk/rucio/user/jue/f3/80/user.jue.13047666.Akt10LCTopoTrmJets._000001.root
#root://eosatlas.cern.ch:1094//eos/atlas/atlasscratchdisk/rucio/user/jue/52/34/user.jue.13047752.Akt10LCTopoTrmJets._000001.root
#outputdir1=root://atlasxrootd-kit.gridka.de:1094//pnfs/gridka.de/atlas/disk-only/atlasscratchdisk/rucio/user/jue/91/6e/user.jue.13047711.Akt10LCTopoTrmJets._000010.root
#/user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000002.root
#outputdir=/eos/user/j/jue/user.jue.mc16_13TeV.361031.Pty8E_A14NNPDF23LO_jetjet_JZ11W.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047695.Akt10LCTopoTrmJets._000011.root


dijet=
#/eos/user/j/jue/user.jue.mc16_13TeV.301493.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M800.DAOD..v31-18-g53934e9_Akt10LCTopoTrmJets/user.jue.13352168.Akt10LCTopoTrmJets._000004.root
#/eos/user/j/jue/user.jue.mc16_13TeV.361031.Pty8E_A14NNPDF23LO_jetjet_JZ11W.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047695.Akt10LCTopoTrmJets._000011.root

outputdir=/eos/user/j/jue/user.jue.mc16_13TeV.301327.Pty8E_A14NNPDF23LO_zprime1500_tt.DAOD..20180223ttbar-34-gb5e402e_Akt10LCTopoTrmJets/user.jue.14152970.Akt10LCTopoTrmJets._000007.root
outputdir1=/eos/user/j/jue/user.jue.mc16_13TeV.301503.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M2000.DAOD..20180223ttbar-37-g7f8e471_Akt10LCTopoTrmJets/user.jue.14176388.Akt10LCTopoTrmJets._000001.root
outputdir2=/eos/user/j/jue/user.jue.mc16_13TeV.301503.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M2000.DAOD..20180223ttbar-37-g7f8e471_Akt10LCTopoTrmJets/user.jue.14176388.Akt10LCTopoTrmJets._000002.root
outputdir3=/eos/user/j/jue/user.jue.mc16_13TeV.301503.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M2000.DAOD..20180223ttbar-37-g7f8e471_Akt10LCTopoTrmJets/user.jue.14176388.Akt10LCTopoTrmJets._000003.root
outputdir4=/eos/user/j/jue/user.jue.mc16_13TeV.301503.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M2000.DAOD..20180223ttbar-37-g7f8e471_Akt10LCTopoTrmJets/user.jue.14176388.Akt10LCTopoTrmJets._000004.root
outputdir5=/eos/user/j/jue/user.jue.mc16_13TeV.301503.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M2000.DAOD..20180223ttbar-37-g7f8e471_Akt10LCTopoTrmJets/user.jue.14176388.Akt10LCTopoTrmJets._000005.root

#eos/user/j/jue/user.jue.mc16_13TeV.301488.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M300.DAOD..v31-18-g53934e9_Akt10LCTopoTrmJets/user.jue.13352149.Akt10LCTopoTrmJets._000001.root

#/eos/user/j/jue/user.jue.mc16_13TeV.301493.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M800.DAOD..v31-18-g53934e9_Akt10LCTopoTrmJets/user.jue.13352168.Akt10LCTopoTrmJets._000004.root 
#user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000001.root
#user.jue.mc16_13TeV.301331.Pty8E_A14NNPDF23LO_zprime2500_tt.DAOD..20180223ttbar-28-gd2bd53a_Akt10LCTopoTrmJets/user.jue.13597428.Akt10LCTopoTrmJets._000079.root 
#user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000001.root
#/eos/user/j/jue/user.jue.mc16_13TeV.301493.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M800.DAOD..v31-18-g53934e9_Akt10LCTopoTrmJets/user.jue.13352168.Akt10LCTopoTrmJets._000004.root
#user.jue.mc16_13TeV.301495.MadPty8E_A14NNPDF23LO_RSGhh_bbbb_c10_M1000.DAOD..v31-10-g5d148a1_Akt10LCTopoTrmJets/user.jue.13047643.Akt10LCTopoTrmJets._000001.root 
#user.jue.mc16_13TeV.410501.PowPty8Ea14_ttbar_h258p75_nonallhad.DAOD..v31-14-g0a89f31_Akt10LCTopoTrmJets/user.jue.13320003.Akt10LCTopoTrmJets._000615.root #@user.jue.180220_v1_2ExktCOM.mc16_13TeV.410501.PowPty8Ea14_ttbar_h258p75_nonallhad.DAOD.e5458_s3126_r9364_r9315_EXT0/user.jue.13262395.EXT0._221515.DAOD_FTAG5.pool.root


#root -l $outputdir/flav_Akt10LCTopoTrmJets_hhbbbb.root  
#python list_branch.py  --f $outputdir --t  bTag_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets 
python jetTree.py --filename $outputdir --sig 0 --specify 1 --saveAs sig  #--num num_M1000.txt   --sig 1 #python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir --sig 1 --specify 1 --saveAs sig  #--num num_M1000.txt   --sig 1 #python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir\hhbbbb_M1500.root  --saveAs sig_M1500  --num num_M1500.txt   --sig 1 
#python jetTree.py --filename $outputdir\hhbbbb_M2000.root  --saveAs sig_M2000  --num num_M2000.txt   --sig 1 
#python jetTree.py --filename $outputdir\ttbar.root         --saveAs bkg_ttbar  --num num_ttbar.txt    #python jetTree.py --filename $outputdir\jetjet.root        --saveAs bkg_jetjet --num num_jetjet.txt   
##ipython trainBDT_split.py --sigfilenames  sig.root -     -bkdfilenames bkg.root
